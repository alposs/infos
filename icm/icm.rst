.. index::
   pair: Echirolles ;  Institut de la communication et des médias
   ! Institut de la communication et des médias

.. icm:

============================================================================
**L'Institut de la communication et des médias (ICM)**
============================================================================

- https://infocom.univ-grenoble-alpes.fr/l-icm/l-icm-479830.kjsp

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.716471374034882%2C45.147871503161355%2C5.720011889934541%2C45.149543747637715&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.14871/5.71824">Afficher une carte plus grande</a></small>


::

    11 Avenue du 8 Mai 1945
    38130 Échirolles


Présentation de l'ICM
===========================

- https://llasic.univ-grenoble-alpes.fr/menu-principal/departements/departement-des-sciences-de-l-information-et-de-la-communication-40296.kjsp
- https://llasic.univ-grenoble-alpes.fr/menu-principal/sites-d-implantation/echirolles/le-campus-d-echirolles-369730.kjsp?RH=LLASIC-FR014
- https://llasic.univ-grenoble-alpes.fr/menu-principal/departements/departement-journalisme-35102.kjsp?RH=5611139540849041

L'Institut de la communication et des médias (ICM) se situe sur la commune
d'Échirolles et dépend de l'Université Grenoble Alpes (UGA).
Le bâtiment abrite :

- `Le département des Sciences de l’information et de la communication <https://infocom.univ-grenoble-alpes.fr/l-icm/departement-des-sciences-de-l-information-et-de-la-communication-40296.kjsp?RH=5611139540849041>`_
- `L’École de Journalisme de Grenoble <https://llasic.univ-grenoble-alpes.fr/menu-principal/departements/departement-journalisme-35102.kjsp?RH=5611139540849041>`_
- `Le laboratoire de recherche GRESEC <https://gresec.univ-grenoble-alpes.fr/>`_

Le département des Sciences de l’information et de la communication et le
département Journalisme font partie de l'UFR LLASIC (Langage, Lettres,
Arts du spectacle, Information et communication, Journalisme), une
des 24 composantes de l'UGA.


Fresque sur la façade de l’Institut de la Communication et des Médias (ICM) d'Echirolles
====================================================================================================

.. figure:: images/facade.webp

   https://www.echirolles.fr/actualites/les-monkey-bird-senvolent (2019)

Cinq jours, du vendredi 22 au mardi 26 novembre 2019, afin de peaufiner les
derniers détails, c’est le temps qu’il aura fallu au duo d’artistes réunis
au sein des **Monkey Bird**, Édouard Egéa et Louis Boidron, pour marquer la Ville
de leur empreinte.

La Ville, ou plutôt la façade de l’Institut de la communication et des médias (ICM)
qui arbore désormais l’une de leurs œuvres : **“Un singe rêveur, méditatif, porté
par un oiseau pour atteindre ses rêves**.

Des animaux qui symbolisent la relation du corps et l’esprit afin de trouver
un équilibre entre les deux”, décrit Louis Boidron, pour qui “ça a été un
plaisir de travailler pour la troisième fois à Grenoble, une ville inspirante”.

A l’image “des montagnes qui nous entourent”. Une œuvre dont le duo espère
désormais “que vous vous l’approprierez...”

Et c’est visiblement déjà le cas pour Jean-Philippe de Oliveira, directeur
du département information-communication de l’Université Grenoble Alpes,
porteuse du projet avec le centre d’art Spacejunk, Grenoble Alpes Métropole
et la Ville d’Échirolles.
“Votre projet a fait consensus au sein du comité d’usagers du bâtiment qui a
travaillé sur le projet. Ça a été direct, rapide. J’ai eu l’image en fond
d’écran durant des semaines, j’avais hâte de découvrir le résultat...”

Une impatience partagée par l’adjointe à la culture Jacqueline Madrennes,
pour qui “ça a été un plaisir partagé. Votre création créé une interface
entre le dedans, l’ICM, et le dehors, l’espace public.

Elle ne vous appartient déjà plus tout à fait. Elle vit dans nos têtes et
appartient à tous les passants...” Un bel envol !


L'UFR des sciences de l'information et de la communication
=================================================================

- https://llasic.univ-grenoble-alpes.fr/menu-principal/departements/departement-des-sciences-de-l-information-et-de-la-communication-40296.kjsp?RH=LLASIC-FR013

.. figure:: images/ufr_sciences_communication.webp
.. figure:: images/departement.webp

.. figure:: images/organigramme.webp

   https://llasic.univ-grenoble-alpes.fr/menu-principal/equipe-et-contacts/organigramme-llasic-page-1-web-801147.kjsp?RH=1455893948716

Le département des Sciences de l’information et de la communication (SIC)
est situé dans le bâtiment de l’Institut de la Communication et des Médias (ICM)
à Echirolles.

Il compte 400 étudiants, 65 doctorants, 38 enseignants-chercheurs, 9 agents
administratifs et techniques.

Les apports des formations du département des Sciences de l'information et
de la communication permettent d’appréhender les stratégies et les pratiques
de communication tout en menant une réflexion sur leurs contextes d’évolution
(sociaux, politiques, économiques, etc.) et leurs rapports avec les publics.


Exposition **Retour vers le futur : l’informatique de 1950 à aujourd’hui**
===============================================================================

- https://culture.univ-grenoble-alpes.fr/menu-principal/patrimoines/patrimoine-scientifique/jardins-et-espaces-museographiques/exposition-retour-vers-le-futur-l-informatique-de-1950-a-aujourd-hui--1312385.kjsp
- https://www.aconit.org/spip/
- https://infocom.univ-grenoble-alpes.fr/l-icm/l-icm-479830.kjsp
- https://www.inria.fr/fr/centre-inria-universite-grenoble-alpes

Cette exposition regroupe une quinzaine de machines, de 1950 au début des
années 2000, afin de présenter une partie de l’histoire de l’informatique.

Ces objets, issus de la collection de l’ACONIT et de la collection universitaire
de l’Institut de la Communication et des Médias, témoignent des innovations
et des expérimentations qui ont marqué notre manière de communiquer et de
calculer depuis 70 ans.

Vous y découvrirez notamment les premiers micro-ordinateurs à destination du
grand public, d’anciennes machines de montage son et vidéo ou encore les
premières machines de traitement de données informatique !


Cette exposition retrace donc une partie de l'histoire de l'informatique.
Pour approfondir la question de l'informatique actuelle à Grenoble,
découvrez le `site du Centre Inria de l'UGA <https://www.inria.fr/fr/centre-inria-universite-grenoble-alpes>`_ dont les projets de recherche et
d'innovation touchent toutes les dimensions des sciences et technologies
du numérique.


Banc de montage
------------------

.. figure:: images/1_banc_de_montage.webp
.. figure:: images/1_le_banc.webp

Banc de montage atlas
-------------------------

.. figure:: images/2_banc_de_montage_atlas.webp
.. figure:: images/2_le_banc_de_montage.webp
.. figure:: images/3_les_perforateurs.webp
.. figure:: images/3_perforateur_de_bande.webp
.. figure:: images/4_la_perforatrice.webp
.. figure:: images/4_perforatrice_de_cartes.webp
.. figure:: images/5_le_teleimprimeur.webp
.. figure:: images/5_teleimprimeur.webp

Caméras
-----------

.. figure:: images/6_cameras.webp

Minitel
-------------

.. figure:: images/7_le_minitel.webp
.. figure:: images/7_minitel.webp

Le macintosh a été utilisé jusqu'en 2001 à l'UGA
------------------------------------------------------------

.. figure:: images/8_le_macinytosh.webp
.. figure:: images/8_mac_introsh.webp


Pour les plus curieux, vous trouverez ci-dessous plusieurs ressources supplémentaires
=========================================================================================

- `Découvrez le musée virtuel de l’ACONIT en cliquant <https://db.aconit.org/dbgalerie/index.php?db=0>`_
- Retrouvez un article sur l’histoire de la micro-informatique à `télécharger <https://culture.univ-grenoble-alpes.fr/medias/fichier/gilles-garel-les-communautes-aux-origines-de-la-micro-informatique-des-amateurs-aux-entreprises-2020_1701271233573-pdf?ID_FICHE=1390232&INLINE=FALSE>`_
- Pour en savoir plus sur l’aventure Apple, cliquez ici
- Apprenez-en plus sur le Minitel ici
- Le Micral N, le premier micro-ordinateur de l’histoire, article à télécharger

Voici deux articles Echosciences spécifiquement dédiés au calcul scientifique
à Grenoble :

- `L'impulsion du traitement des données de calcul en sciences physiques à Grenoble de 1960 à 1990 : à lire <https://www.echosciences-grenoble.fr/articles/l-impulsion-du-traitement-des-donnees-de-calcul-en-sciences-physiques-a-grenoble-de-1960-a-1990-de-la-mecanographie-a-l-informatique>`_
- `Recherche, informatique, industrie : La saga de l'informatique scientifique à Grenoble : à lire ici <https://www.echosciences-grenoble.fr/articles/recherche-informatique-industrie-la-sage-de-l-informatique-scientifique-a-grenoble-3-10-annee-2021>`_

Regardez l'actualité du `Gresec <https://gresec.univ-grenoble-alpes.fr/actualites>`_ !




