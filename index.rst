.. index::
   ! AlpOSS infos

.. raw:: html

    <a rel="me" href="https://babka.social/@pvergain"></a>
    <a rel="me" href="https://framapiag.org/@pvergain"></a>

.. un·e

|FluxWeb| `RSS <https://alposs.frama.io/infos/rss.xml>`_


.. _alposs_2024:

================================================
**AlpOSS infos**
================================================

- https://alposs.fr/
- https://alposs.frama.io/alposs-2025/
- https://alposs.frama.io/alposs-2024/

.. toctree::
   :maxdepth: 5

   people/people
   logiciels/logiciels   
   icm/icm
