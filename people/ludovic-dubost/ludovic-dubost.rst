.. index::
   ! Ludovic Dubost

.. _ludovic_dubost:

============================================================================
**Ludovic Dubost**
============================================================================


Biographie
===============

Ludovic Dubost, XWiki

Créateur d'XWiki et CEO de XWiki SAS, il développe l'entreprise du libre 
XWiki SAS depuis 20 ans, ainsi que membre du board de l'association Open Food Facts.

XWiki SAS, est une entreprise Européenne basée en France et en Roumanie, 
développe uniquement des logiciels Open Source applicatifs dans le domaine 
du partage de connaissance et la collaboration sécurisée. 

XWiki est utilisé par des milliers d'organisations afin d'organiser, partager 
et collaborer sur l'information. 

XWiki SAS développe aussi le logiciel CryptPad, une solution innovante 
permettant la collaboration tout en protégeant la confidentialité de ses données.

Il participe depuis plus de 10 ans et présente aux conférences Open Source 
Experience, FOSDEM, OW2Con, Capitole du Libre aussi bien sur les logiciels 
de collaboration ou de vie privée que sur les business models et le financement 
des logiciels Open Source.


Interventions 2025
=======================

- :ref:`alposs_2025:ludovic_dubost_2025_02_20`

