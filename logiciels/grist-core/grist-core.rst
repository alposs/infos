.. index::
   pair: Grist; a modern relational spreadsheet

.. _grist_core:

=======================================================================================================================================
**Grist-core** (a modern relational spreadsheet. It combines the flexibility of a spreadsheet with the robustness of a database)
=======================================================================================================================================

- https://github.com/gristlabs
- https://github.com/gristlabs/grist-core
- https://github.com/gristlabs/grist-core/releases.atom
- https://fosstodon.org/@grist
- https://fosstodon.org/@grist.rss

Documentation
=================

- https://support.getgrist.com/fr/
- https://support.getgrist.com/



Articles 2025
==================

- :ref:`alposs_2025:aurelie_jallut_2025_02_20`
- :ref:`alposs_2025:jimmy_levacher_2025_02_20`
- https://korben.info/grist-tableur-base-de-donnees-collaboratif.html
- https://pouet.chapril.org/@Greguti/113967346957860340
