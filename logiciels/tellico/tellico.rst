.. index::
   ! tellico

.. _tellico:

=============================================================================================================================
**tellico** (application pour organiser vos collections)
=============================================================================================================================

- https://apps.kde.org/fr/tellico/
- https://tellico-project.org/
- https://invent.kde.org/office/tellico


Articles 2025
=================

- :ref:`alposs_2025:bruno_cornec_2025_02_20`
